//
//  Server.h
//  kinectAtsugi
//
//  Created by kikko on 20/04/14.
//
//

#ifndef __kinectAtsugi__Server__
#define __kinectAtsugi__Server__

#include "ofMain.h"
#include "ofxLibwebsockets.h"
#include "Params.h"

class ActionEventArgs {
public:
    ActionEventArgs(string _name, Json::Value _data)
    :name(_name),data(_data){}

    string name;
    Json::Value data;
};


class Server {
    
public:
    
    ~Server();
    
    void setup(Params::Ptr params);
    void update();
    
    void openInBrowser();
    
    void onConnect( ofxLibwebsockets::Event& args );
    void onOpen( ofxLibwebsockets::Event& args );
    void onClose( ofxLibwebsockets::Event& args );
    void onIdle( ofxLibwebsockets::Event& args );
    void onMessage( ofxLibwebsockets::Event& args );
    void onBroadcast( ofxLibwebsockets::Event& args );
    
    void send(string data);
    void parameterChanged( ofAbstractParameter & parameter );
    
    ofEvent<ActionEventArgs> actionEvt;
    
private:
    
    void setParam(const Json::Value & json);
    
    Params::Ptr params;
    ofxLibwebsockets::Server server;

    bool bUpdatingParams;
    bool bConnected;
};

#endif /* defined(__kinectAtsugi__Server__) */
