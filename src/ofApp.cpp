#include "ofApp.h"

void ofApp::setup() {
	ofBackground(255);
    ofSetFrameRate(60);
    ofDisableArbTex();
	
    params = Params::create();
    params->setup();
    ofAddListener(params->parameterChangedE, this, &ofApp::paramsUpdated);
    
    preferedSimplification = params->simplification;
    
    server.setup(params);
    ofAddListener(server.actionEvt, this, &ofApp::actionAsked);
    
	for (int i=0; i<1; i++) {
        KinectMesh * km = new KinectMesh();
        km->setup(i, params);
        kinectMeshes.push_back( km );
    }
    
    postProc.setup();
    
    anim.setup(params, &camera);
    
//    cam.setDrag(0.9999);
    camera.setup();
    
    overlay.setup();
    
    for (auto & km : kinectMeshes) {
        km->update();
    }
    
    bGrabScreenshot = false;
    screenShotFBO.allocate(ofGetWidth(), ofGetHeight(), GL_RGBA, 1);
}

void ofApp::update() {
    
    bool bForceRefresh = false;
    
    for(auto parameter : updatedParams) {
        bool bParamIsSimplification = parameter->getName() == params->simplification.getName();
        bool bParamIsDistMax = parameter->getName() == params->simplification.getName();
        if( bParamIsSimplification || bParamIsDistMax) {
            bForceRefresh = true;
            if( bParamIsSimplification ) {
                preferedSimplification = params->simplification;
            }
        }
        else if (parameter->getName() == params->scanAnim.getName()) {
            bool val = parameter->cast<bool>().get();
            if (val) {
                params->simplification = 2;
                camera.storePosition();
//                camera.speed = 0.01;
                anim.start();
            } else{
                params->scanDeform = 0;
                params->simplification = preferedSimplification;
                server.parameterChanged(params->scanDeform);
                anim.stop();
                camera.restorePosition();
            }
            server.parameterChanged(params->simplification);
            bForceRefresh = true;
        }
    }
    updatedParams.clear();
    
    if(!params->scanAnim || bForceRefresh) {
        for (auto & km : kinectMeshes) {
            km->update();
        }
    }
    
    anim.update();
    camera.update();
}

void ofApp::draw() {
    
	ofSetColor(255, 255, 255);
    
    postProc.begin();
    
    camera.begin();
    
    if(params->debugMode){
        anim.drawDebug();
    }
    
    for (auto & km : kinectMeshes) {
        km->draw(true);
    }
    
    camera.end();
    
    postProc.end();
    
    postProc.draw();
    
    overlay.draw(params->mainColor, bGrabScreenshot);
    
    if(bGrabScreenshot) {
        grabScreenshot();
        bGrabScreenshot = false;
    }
    
    if(params->debugMode){
        
        ofSetColor(params->mainColor);
        int pos = -65;
        ostringstream oss;
        oss << ofToString(ofGetFrameRate(), 1) << " fps" << endl;
        oss << "SPACE to switch " << (params->scanAnim?"OFF":"ON") << " Scan Animation" << endl;
        oss << "'w' to open Web UI" << endl;
        oss << "'f' to toggle fullscreen" << endl;
        oss << "'s' to hide/show this notice" << endl;
        if(!params->scanAnim) {
            oss << "Drag to rotate - ALT + Drag to pan - CTRL + Drag to dolly"<< endl;
            pos -= 15;
        }
        ofDrawBitmapString(oss.str(), 10, ofGetHeight()+pos);
    }
}

void ofApp::grabScreenshot() {
    
    string filename = "export/" + ofGetTimestampString() + "_" + screenshotName + ".png";
    
    ofImage img;
    img.grabScreen(0, 0, ofGetWidth(), ofGetHeight());
    img.saveImage(filename);
    
    Json::Value json;
    json["type"] = "event";
    json["name"] = "screenshot";
    json["data"] = filename;
    server.send(Json::FastWriter().write(json));
}

void ofApp::paramsUpdated(ofAbstractParameter & parameter) {
    updatedParams.push_back(&parameter);
}

void ofApp::actionAsked(ActionEventArgs & ev) {
    if(ev.name=="screenshot"){
        bGrabScreenshot = true;
        screenshotName = ev.data.asString();
    }
}

void ofApp::windowResized(int w, int h){
    postProc.resize(w, h);
}

void ofApp::keyPressed (int key) {
	switch (key) {
		case 'f':
			ofToggleFullscreen();
			break;
		case 's':
            if (ofGetKeyPressed(OF_KEY_COMMAND)) {
                for (auto & km : kinectMeshes) {
                    km->update();
                }
                kinectMeshes[0]->save("saved/test2");
            }
			else {
                params->debugMode = !params->debugMode;
//              updatedParams.push_back(&params->debugMode);
                server.parameterChanged(params->debugMode);
            }
			break;
		case 'z': case 'w':
			server.openInBrowser();
			break;
		case ' ':
			params->scanAnim = !params->scanAnim.get();
            updatedParams.push_back(&params->scanAnim);
            server.parameterChanged(params->scanAnim);
			break;
	}
}