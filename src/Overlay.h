//
//  Overlay.h
//  kinectAtsugi
//
//  Created by kikko on 23/04/14.
//
//

#ifndef kinectAtsugi_Overlay_h
#define kinectAtsugi_Overlay_h

#include "ofMain.h"

class Overlay {
    
public:
    
    void setup(){
        ofTrueTypeFont::setGlobalDpi(72);
        font.loadFont("assets/frabk.ttf", 32);
        font.setLineHeight(34);

        logo.loadImage("assets/logo.png");
        scale = 0.2;
    }
    
    void draw(const ofColor & color, bool bMarginForPrint = false){
        float w = (float)ofGetWidth() * scale;
        float mX, mY;
        mX = (float)ofGetWidth() * 0.04;
        mY = (float)ofGetWidth() * 0.02;
        if(bMarginForPrint) {
            mX = (float)ofGetWidth() * 0.12;
        }
        logo.draw(mX, mY, scale, w, w * logo.getHeight() / (float)logo.getWidth());
        
        ofPushStyle();
        ofSetColor(color);
        
        ostringstream buf;
        buf << "#RealHumans" << endl << "#Atsugi" << endl << "@Artefr";
        string text = buf.str();
        
        ofRectangle rect = font.getStringBoundingBox(text, 0, 0);
        font.drawString(text, ofGetWidth() - rect.width - mX, ofGetHeight() - rect.height - mY );
        
        ofPopStyle();
    }
    
private:
    ofImage logo;
    ofTrueTypeFont font;
    float scale;
};

#endif
