//
//  Params.h
//  kinectAtsugi
//
//  Created by kikko on 20/04/14.
//
//

#ifndef kinectAtsugi_Params_h
#define kinectAtsugi_Params_h

#include "ofMain.h"
#include "json/json.h"


class Params : public ofParameterGroup {
    
public:
    
    // in web ui group
    ofParameter<int>   simplification;
    ofParameter<int>   distMax;
    ofParameter<float> kinectOffsetZ;
    ofParameter<float> textureAlphaOffset;
    ofParameter<float> textureAlphaOffsetScan;
    ofParameter<bool>  scanAnim;
    ofParameter<bool>  loopScanAnim;
    ofParameter<bool>  debugMode;
    ofParameter<float> animWidth;
    ofParameter<float> animHeight;
    ofParameter<float> animDepth;
    
    // out
    ofParameter<float> scanDeform;
    
    ofColor mainColor;
    
//    ofEvent<string> actionEvt;
    
    void setup() {
        setName("KinectMesh");
        add( scanAnim.set("Scan Animation", false) );
        add( loopScanAnim.set("Loop Scan Animation", false) );
        add( debugMode.set("Debug View", true) );
        add( simplification.set("Simplification", 4, 1, 15) );
        add( distMax.set("Distance Max", 1200, 1000, 3000) );
        add( kinectOffsetZ.set("Kinect Offset Z", 1000.0, 0.0, 2000.0) );
        add( textureAlphaOffset.set("Texture alpha offset", 0.0, 0.0, 1.0) );
        add( textureAlphaOffsetScan.set("Texture alpha offset Scan", 0.0, 0.0, 1.0) );
        
        add( animWidth.set("Anim Box Width", 300, 50, 1000) );
        add( animHeight.set("Anim Box Height", 300, 50, 1000) );
        add( animDepth.set("Anim Box Depth", 100, 50, 1000) );
        
        scanDeform.set("Scan Deform", 0.0, 0.0, 1.0);
        
        mainColor.set(41, 190, 194);
    }
    
    Json::Value getJSON() {
        Json::Value arr;
        for(int i=0; i<size(); i++) {
            ofAbstractParameter & p = get(i);
            Json::Value v = Json::Value();
            v["name"] = p.getName();
            if( p.type() == typeid(ofParameter<int>).name() ){
                auto intP = p.cast<int>();
                v["min"] = intP.getMin();
                v["max"] = intP.getMax();
                v["type"] = "int";
                v["value"] = intP.get();
            } else if( p.type() ==typeid(ofParameter<float>).name() ){
                auto floatP = p.cast<float>();
                v["min"] = floatP.getMin();
                v["max"] = floatP.getMax();
                v["type"] = "float";
                v["value"] = floatP.get();
            } else if( p.type() == typeid(ofParameter<string>).name() ){
                v["type"] = "string";
                v["value"] = p.cast<string>().get();
            } else if( p.type() == typeid(ofParameter<bool>).name() ){
                v["type"] = "bool";
                v["value"] = p.cast<bool>().get();
            }
            arr[i] = v;
        }
        Json::Value json;
        json["group"] = getName();
        json["params"] = arr;
        return json;
    }
    
    typedef shared_ptr<Params> Ptr;
    static Ptr create() { return shared_ptr<Params>(new Params); }
};

#endif
