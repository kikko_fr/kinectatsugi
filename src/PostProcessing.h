//
//  PostProcessing.h
//  kinectAtsugi
//
//  Created by kikko on 19/04/14.
//
//

#ifndef __kinectAtsugi__PostProcessing__
#define __kinectAtsugi__PostProcessing__

#include "ofMain.h"

class PostProcessing {
    
public:
    void setup();
    void begin();
    void end();
    
    void draw();
    void resize(int w, int h);
    
    ofFbo & getFbo() { return fbo; }

private:
    ofFbo fbo;
    ofShader tiltshift;
    
    void texturedQuad(float x, float y, float width, float height, float s = 1.0, float t = 1.0);
};

#endif /* defined(__kinectAtsugi__PostProcessing__) */
