//
//  KinectMesh.h
//  kinectAtsugi
//
//  Created by kikko on 18/04/14.
//
//

#ifndef __kinectAtsugi__KinectMesh__
#define __kinectAtsugi__KinectMesh__

#include "ofMain.h"
#include "ofxKinect.h"
#include "Params.h"


#define USE_KINECT


class KinectSim {
    
public:
    void load(string filename);
    float getDistanceAt(int x, int y);
    ofVec3f getWorldCoordinateAt(int x, int y);
    ofTexture & getTextureReference();
    bool isFrameNew();
    
private:
    ofImage tex;
    ofVec3f coords[640*480];
};





class KinectMesh {
public:
    
    KinectMesh();
    ~KinectMesh();
    
#ifdef USE_KINECT
    ofxKinect kinect;
#else
    KinectSim kinect;
#endif
    
    ofVboMesh mesh;
    ofTexture wireframeTex;
    
    ofShader scanShader;
    
//    void setup(string filename);
    void setup(int kinectId, Params::Ptr params);
    void update();
    void draw( bool bWireframeOverlay);
    
//    void load(string name);
    void save(string name);
    
    int triangleMaxLength;
    
    float minZ, maxZ;
    
private:
    Params::Ptr params;
};

#endif /* defined(__kinectAtsugi__KinectMesh__) */
