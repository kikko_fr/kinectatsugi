//
//  KinectMesh.cpp
//  kinectAtsugi
//
//  Created by kikko on 18/04/14.
//
//

#include "KinectMesh.h"


KinectMesh::KinectMesh() {
    triangleMaxLength = 50;
    
    minZ = 9999;
    maxZ = -9999;
}

KinectMesh::~KinectMesh() {
    
#ifdef USE_KINECT
    kinect.close();
#endif
}

void KinectMesh::setup(int kinectId, Params::Ptr params) {
	
    this->params = params;
    
#ifdef USE_KINECT
    kinect.setRegistration(true);
	kinect.init();
	kinect.open(kinectId);
	
	// print the intrinsic IR sensor values
	if(kinect.isConnected()) {
		ofLogNotice() << "sensor-emitter dist: "   << kinect.getSensorEmitterDistance() << "cm";
		ofLogNotice() << "sensor-camera dist:  "   << kinect.getSensorCameraDistance()  << "cm";
		ofLogNotice() << "zero plane pixel size: " << kinect.getZeroPlanePixelSize()    << "mm";
		ofLogNotice() << "zero plane dist: "       << kinect.getZeroPlaneDistance()     << "mm";
	}
#else
	kinect.load("saved/test");
#endif
    
    ofPixels px;
    px.allocate(100, 100, 4);
    px.set(0, params->mainColor.r);
    px.set(1, params->mainColor.g);
    px.set(2, params->mainColor.b);
    px.set(3, 255);
    wireframeTex.allocate(100, 100, GL_RGBA);
    wireframeTex.loadData(px);
    
    scanShader.load("shaders/scan.vert", "shaders/scan.frag");//, "shaders/scan.geom");
    
    mesh.setMode(OF_PRIMITIVE_TRIANGLES);
}

void KinectMesh::update() {
    
#ifdef USE_KINECT
    kinect.update();
    if (!kinect.isFrameNew()) return;
#endif

    mesh.clear();
    
    int step = params->simplification;
    float maxl = triangleMaxLength;
    
    float d1, d2, d3, d4;
    ofPoint p1, p2, p3, p4;
    bool t1Valid=true, t2Valid=true;
    bool pt1Valid, pt2Valid;
    
    int w = 640;
    int h = 480;
    for(int y = 0; y < h - step; y += step) {
        for(int x = 0; x < w - step; x += step) {
            d1 = kinect.getDistanceAt(x, y);
            d2 = kinect.getDistanceAt(x+step, y);
            d3 = kinect.getDistanceAt(x, y+step);
            d4 = kinect.getDistanceAt(x+step, y+step);
            p1 = kinect.getWorldCoordinateAt(x, y);
            p2 = kinect.getWorldCoordinateAt(x+step, y);
            p3 = kinect.getWorldCoordinateAt(x, y+step);
            p4 = kinect.getWorldCoordinateAt(x+step, y+step);
            t1Valid = d1 > 0 && d1 < params->distMax && d2 > 0 && d2 < params->distMax && d3 > 0 && d3 < params->distMax;
            t2Valid = d2 > 0 && d2 < params->distMax && d3 > 0 && d3 < params->distMax && d4 > 0 && d4 < params->distMax;
            pt1Valid = abs(p1.z-p2.z) < maxl && abs(p1.z-p3.z) < maxl && abs(p2.z-p3.z) < maxl;
            pt2Valid = abs(p2.z-p3.z) < maxl && abs(p2.z-p4.z) < maxl && abs(p3.z-p4.z) < maxl;
            if( t1Valid && pt1Valid) {
                mesh.addTexCoord(ofVec2f(x, y));
                mesh.addVertex(p1);
                mesh.addTexCoord(ofVec2f(x+step, y));
                mesh.addVertex(p2);
                mesh.addTexCoord(ofVec2f(x, y+step));
                mesh.addVertex(p3);
            }
            if( t2Valid && pt2Valid) {
                mesh.addTexCoord(ofVec2f(x, y+step));
                mesh.addVertex(p3);
                mesh.addTexCoord(ofVec2f(x+step, y));
                mesh.addVertex(p2);
                mesh.addTexCoord(ofVec2f(x+step, y+step));
                mesh.addVertex(p4);
            }
        }
    }
    
    auto & uvs = mesh.getTexCoords();
    for (auto &uv : uvs) {
        uv.x /= 640;
        uv.y /= 480;
    }
}

void KinectMesh::draw(bool bWireframeOverlay) {
    
    ofPushMatrix();
    ofPushStyle();
    
    ofScale(1, -1, -1);
    ofTranslate(0, 0, -params->kinectOffsetZ);
    
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    
    scanShader.begin();
    scanShader.setUniform1f("time", (ofGetElapsedTimef()) * 0.05 );
    scanShader.setUniform2f("deform", 0, params->scanDeform);
    
    ofEnableAlphaBlending();
    
    float pct;
    if(params->scanAnim)
        pct = 1-params->scanDeform-params->textureAlphaOffsetScan;
    else
        pct = 1-params->scanDeform-params->textureAlphaOffset;
    
    scanShader.setUniform1f("alpha", pct);
    scanShader.setUniform3f("colorOffset", 0, 0, 0);
    kinect.getTextureReference().bind();
    mesh.drawFaces();
    kinect.getTextureReference().unbind();
    
    ofTranslate(0, 0, -0.1);
    
    wireframeTex.bind();
    scanShader.setUniform1f("alpha", (float)params->simplification / 5);
    ofFloatColor c = (ofColor::white - params->mainColor) * pct;
    scanShader.setUniform3f("colorOffset", c.r, c.g, c.b);
    if(bWireframeOverlay) mesh.drawWireframe();
    ofDisableBlendMode();
    wireframeTex.unbind();
    
    scanShader.end();

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    
    ofPopStyle();
    ofPopMatrix();
}

void KinectMesh::save(string filename) {
    
#ifdef USE_KINECT
    ofImage img;
    img.setFromPixels(kinect.getPixelsRef());
    img.saveImage(filename+".png");
    
    int w = 640, h = 480, size = w * h * 3, i = 0;
    float buffer[size];
    ofVec3f p;
    for(int y = 0; y < h; y++) {
        for(int x = 0; x < w; x++) {
            p = kinect.getWorldCoordinateAt(x, y);
            buffer[i]   = p.x;
            buffer[i+1] = p.y;
            buffer[i+2] = p.z;
            i+=3;
        }
    }
    ofFile file;
    file.open(filename+".depth", ofFile::WriteOnly, true);
    file.write( reinterpret_cast<char *>(buffer), sizeof(float) * w * h * 3 );
    file.close();
#endif
}



///------------

void KinectSim::load(string filename){
    tex.loadImage(filename+".png");
    
    ofBuffer buf = ofBufferFromFile(filename+".depth", true);
    char * bin = buf.getBinaryBuffer();
    float * data = reinterpret_cast<float *>(bin);
    ofVec3f p;
    for (int i=0; i<640*480; i++) {
        p.x = data[i*3];
        p.y = data[i*3+1];
        p.z = data[i*3+2];
        coords[i] = p;
    }
}

float KinectSim::getDistanceAt(int x, int y){
    return coords[y*640+x].z;
}

ofVec3f KinectSim::getWorldCoordinateAt(int x, int y){
    return coords[y*640+x];
}

ofTexture & KinectSim::getTextureReference(){
    return tex.getTextureReference();
}