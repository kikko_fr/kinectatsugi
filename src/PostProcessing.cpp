//
//  PostProcessing.cpp
//  kinectAtsugi
//
//  Created by kikko on 19/04/14.
//
//

#include "PostProcessing.h"

void PostProcessing::setup(){
    fbo.allocate(ofGetWidth(), ofGetHeight(), GL_RGB, 1);
    tiltshift.load("shaders/tiltshift");
}

void PostProcessing::begin(){
    fbo.begin();
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
}

void PostProcessing::end(){
    fbo.end();
}

void PostProcessing::resize(int w, int h){
    fbo.allocate(w, h, GL_RGB, 1);
}

void PostProcessing::draw(){
    
    tiltshift.begin();
    tiltshift.setUniformTexture("tDiffuse", fbo.getTextureReference(), 0);
    tiltshift.setUniform1f("h", fbo.getHeight());
    tiltshift.setUniform1f("r", 0.4);
    
    fbo.draw((float)(ofGetWidth() -fbo.getWidth())  * 0.5,
             (float)(ofGetHeight()-fbo.getHeight()) * 0.5);
    
    tiltshift.end();
}