#pragma once

#include "ofMain.h"
#include "KinectMesh.h"
#include "Params.h"
#include "Server.h"
#include "PostProcessing.h"
#include "ScanAnimation.h"
#include "Camera.h"
#include "Overlay.h"

class ofApp : public ofBaseApp {
public:
	
	void setup();
	void update();
	void draw();
	
	void keyPressed(int key);
    void windowResized(int w, int h);
	
    bool bDrawWireframe;
    void grabScreenshot();
	
    Camera camera;
    
    Params::Ptr params;
    void paramsUpdated(ofAbstractParameter & parameter);
    void actionAsked(ActionEventArgs & ev);
    
    vector<KinectMesh*> kinectMeshes;
    PostProcessing postProc;
    
    Server server;
    
    ScanAnimation anim;
    
    bool bGrabScreenshot;
    ofFbo screenShotFBO;
    string screenshotName;
    
    int preferedSimplification;
    
    Overlay overlay;
    
    vector<ofAbstractParameter *> updatedParams;
};
