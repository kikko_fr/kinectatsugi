//
//  ScanAnimation.cpp
//  kinectAtsugi
//
//  Created by kikko on 21/04/14.
//
//

#include "ScanAnimation.h"

void ScanAnimation::setup(Params::Ptr params, Camera::Ptr camera){
    this->params = params;
    this->camera = camera;
    speed = 0.0025f;
}

void ScanAnimation::start() {
    camera->setInteractive(false);
    reset();
    bRunning = true;
}

void ScanAnimation::stop() {
    camera->speed = 0.1;
    camera->setInteractive(true);
    bRunning = false;
}

void ScanAnimation::reset() {
    animPct = 0;
    
    randomCamera();
    camera->speed = 1;
    camera->update();
    
    camera->speed = 0.0045;
    if(params->loopScanAnim) {
        randomCamera();
    } else {
        camera->restorePosition();
    }
}

void ScanAnimation::randomCamera() {
    camera->longitude = ofRandomf() * 60;
    camera->latitude  = ofRandomf() * 60;
    camera->distance  = ofRandom(500) + 10;
    camera->target.x  = ofRandomf() * params->animWidth;
    camera->target.y  = ofRandomf() * params->animHeight;
    camera->target.z  = ofRandomf() * params->animDepth;
}

void ScanAnimation::drawDebug(){
    glEnable(GL_DEPTH_TEST);
    
    ofPushMatrix();
    ofPushStyle();
    ofSetColor(params->mainColor);
    
    ofDrawSphere(0, 0, 0, 2);
    ofDrawAxis(30);
    
    ofNoFill();
    ofDrawBox(ofPoint::zero(), params->animWidth*2, params->animHeight*2, params->animDepth*2);
    ofFill();
    
    ofPopMatrix();
    ofPopStyle();
    
    glDisable(GL_DEPTH_TEST);
}

void ScanAnimation::update(){
    if (params->scanAnim && bRunning) {
        animPct += speed;
        if(animPct>1.25) {
            if(params->loopScanAnim) {
                reset();
                params->scanDeform = sin( (animPct + 0.5) * PI) / 2 + 0.5; // fix glitch
            } else {
                stop();
            }
        }
        else if(animPct<1) {
            params->scanDeform = sin( (animPct + 0.5) * PI) / 2 + 0.5;
        }
    }
}