//
//  Server.cpp
//  kinectAtsugi
//
//  Created by kikko on 20/04/14.
//
//

#include "Server.h"

Server::~Server() {
    ofRemoveListener(params->parameterChangedE, this, &Server::parameterChanged);
}

void Server::setup(Params::Ptr params_) {
    
    params = params_;
    ofAddListener(params->parameterChangedE, this, &Server::parameterChanged);
    bUpdatingParams = false;
    
    ofxLibwebsockets::ServerOptions options = ofxLibwebsockets::defaultServerOptions();
    options.port = 3000;
	options.bUseSSL = false; //ssl not working on Win right now!
    bConnected = server.setup( options );
    server.addListener(this);
    
    ofLog() << "server listening on port " << ofToString(options.port);
}

void Server::openInBrowser() {
    
    string url = "http";
    if ( server.usingSSL() ){
        url += "s";
    }
    url += "://localhost:" + ofToString( server.getPort() );
    ofLaunchBrowser(url);
}

void Server::send(string data) {
    server.send( data );
}

void Server::parameterChanged( ofAbstractParameter & p ){
    if(bUpdatingParams) return;
    
    Json::Value json;
    json["type"] = "param_update";
    json["data"] = Json::Value();
    json["data"]["name"] = p.getName();
    
    if( p.type() == typeid(ofParameter<int>).name() ){
        json["data"]["value"] = p.cast<int>().get();
    } else if( p.type() ==typeid(ofParameter<float>).name() ){
        json["data"]["value"] = p.cast<float>().get();
    } else if( p.type() == typeid(ofParameter<string>).name() ){
        json["data"]["value"] = p.cast<string>().get();
    } else if( p.type() == typeid(ofParameter<bool>).name() ){
        json["data"]["value"] = p.cast<bool>().get();
    }
    
    Json::FastWriter writer;
    server.send( writer.write(json) );
}

void Server::setParam(const Json::Value & json) {
    
    ofAbstractParameter & p = params->get( json["name"].asString() );
    
    bUpdatingParams = true;
    
    if( p.type() == typeid(ofParameter<int>).name() ){
        p.cast<int>() = json["value"].asInt();
    } else if( p.type() ==typeid(ofParameter<float>).name() ){
        p.cast<float>() = json["value"].asFloat();
    } else if( p.type() == typeid(ofParameter<string>).name() ){
        p.cast<string>() = json["value"].asString();
    } else if( p.type() == typeid(ofParameter<bool>).name() ){
        p.cast<bool>() = json["value"].asBool();
    }

    bUpdatingParams = false;
}

//---- socket server listeners

void Server::onConnect( ofxLibwebsockets::Event& args ){
    ofLog() << "on connected";
}
void Server::onOpen( ofxLibwebsockets::Event& args ){
    ofLog() << "on open";
    
    Json::Value json;
    json["type"] = "definition";
    json["data"] = params->getJSON();
    Json::FastWriter writer;
    args.conn.send( writer.write(json) );
}
void Server::onClose( ofxLibwebsockets::Event& args ){
    ofLog() << "on close";
}
void Server::onIdle( ofxLibwebsockets::Event& args ){
    ofLog() << "on idle";
}
void Server::onMessage( ofxLibwebsockets::Event& args ){
    Json::Reader reader;
    Json::Value json;
    if(!reader.parse( args.message, json )) {
		ofLogError("JSON Parse ERROR: ") << reader.getFormattedErrorMessages();
		return;
	}
    
    if(json["action"].isString()) {
        string action = json["action"].asString();
        Json::Value data = json["data"];
        ActionEventArgs args = ActionEventArgs(action, data);
        ofNotifyEvent(actionEvt, args);
    }
    else setParam(json);
}
void Server::onBroadcast( ofxLibwebsockets::Event& args ){
    ofLog() << "got broadcast " << args.message;
}