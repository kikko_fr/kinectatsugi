//
//  ScanAnimation.h
//  kinectAtsugi
//
//  Created by kikko on 21/04/14.
//
//

#ifndef __kinectAtsugi__ScanAnimation__
#define __kinectAtsugi__ScanAnimation__

#include "ofMain.h"
#include "Params.h"
#include "Camera.h"

class ScanAnimation {
public:
    void setup(Params::Ptr params, Camera::Ptr camera);
    void update();
    
    void start();
    void stop();
    void reset();
    
    void drawDebug();
    
    float speed;
    
private:
    Params::Ptr params;
    Camera::Ptr camera;
    
    ofVec3f destTarget;
    ofVec3f destRotation;
    
    float animPct;
    
    void randomCamera();
    bool bRunning;
};
#endif /* defined(__kinectAtsugi__ScanAnimation__) */
