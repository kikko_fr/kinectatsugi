#version 150

uniform sampler2D tDiffuse;
uniform bool bUseTexture;
uniform vec3 colorOffset;
uniform float alpha;

in vec2 texCoordVarying;
//in float alpha;

out vec4 fragColor;

void main(){
    vec3 color = texture(tDiffuse, texCoordVarying).rgb + colorOffset;
	fragColor = vec4(color, alpha);
}
