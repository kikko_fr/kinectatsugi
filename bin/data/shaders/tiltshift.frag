#version 150

uniform sampler2D tDiffuse;
uniform float h;
uniform float r;

in vec2 texCoordVarying;

out vec4 fragColor;

void main() {
    vec2 vUv = texCoordVarying;
    vec4 sum = vec4( 0.0 );
    
    float blurAmnt = 2;
    float hh = abs( r - vUv.y ) / h * blurAmnt;
    
    sum += texture( tDiffuse, vec2( vUv.x - 4.0 * hh, vUv.y ) ) * 0.051;
    sum += texture( tDiffuse, vec2( vUv.x - 3.0 * hh, vUv.y ) ) * 0.0918;
    sum += texture( tDiffuse, vec2( vUv.x - 2.0 * hh, vUv.y ) ) * 0.12245;
    sum += texture( tDiffuse, vec2( vUv.x - 1.0 * hh, vUv.y ) ) * 0.1531;
    sum += texture( tDiffuse, vec2( vUv.x, vUv.y ) ) * 0.1633;
    sum += texture( tDiffuse, vec2( vUv.x + 1.0 * hh, vUv.y ) ) * 0.1531;
    sum += texture( tDiffuse, vec2( vUv.x + 2.0 * hh, vUv.y ) ) * 0.12245;
    sum += texture( tDiffuse, vec2( vUv.x + 3.0 * hh, vUv.y ) ) * 0.0918;
    sum += texture( tDiffuse, vec2( vUv.x + 4.0 * hh, vUv.y ) ) * 0.051;
    
    fragColor = sum;
}
