function setup(){
	$container = $('#container');

	// addConnectButton();

	// addGroup('Components List');
	// addSlider('float','floatSlider', 50, 0, 100, 'Float Slider');
	// addSlider('integer', 'intSlider', 50, 0, 100, 'Int Slider');
	// addCheckbox('checkbox', 'Checkbox');
	// addToggle('toggleSwitch', 'Toggle Switch');
	// addSelect('singleSelect', ['Option 1', 'Option 2', 'Option 3', 'etc..'], false, 'Single Select');
	// addSelect('multipleSelect', ['1', '2', '3', '4', '5', '6', '7'], true, 'Multiple Select');

	// addGroup('KinectMesh');
	// addSlider('float','size', 50, 0, 100);
	// addSlider('integer', 'simplification', 4, 1, 15);
	// addCheckbox('check');

	$btConnect = $('#bt_connect');
	$btConnect.click( function (){
		if(bConnected) disconnect();
		else {
			$container.children().each(function(){
				if( $(this).attr('id') != 'ui' ) {
					$(this).remove();
				}
			});
			connect();
		}
	});

	$btScreen = $('#bt_screen');
	$btScreen.click( function(){
		socket.send( JSON.stringify({action:'screenshot', data:$('#name').val()}) );
	});

//	$btPrint = $('#bt_print');
//	$btPrint.click(function(){
//		var src = $('#screenshot > img').attr('src');
//		var name = $('#name').val();
//		var $name = $('<p>').html(name)
//									.css('position', 'absolute')
//									.css('color', 'white')
//									.css('top', '0')
//									.css('left', '1em')
//									.css('font-size', '1.5em')
//									.css('font-family', 'Helvetica, Arial, sans-serif');
//		var html = '<html><body style="margin:0;padding:0;">';
//		html += '<img src="'+ src +'"/>';
//		html += $('<div>').append($name).html();
//		html += '</body></html>';
//
//		var popup = window.open();
//		popup.document.write( html );
//		popup.print();
//	});

	onConnectCallback = function() {
		$('#bt_screen').attr('disabled', false);
		// if ( $('#screenshot > img').length )
		//   $('#bt_print').attr('disabled', false);
	};
	onDisonnectCallback = function() {
		$('#ui .btn').attr('disabled', true);
	};
	onEvent = function(name, data) {
		if (name=="screenshot") {
			$('#screenshot')
				.html('')
				.append( $('<img>').attr('src', data).attr('height', 300));
		}
		// $btPrint.attr("disabled", false);
	};
	
	disable();

	$('body').css('visibility', 'visible');

	connect();
}

$(document).ready( function() {
	setup();
});

