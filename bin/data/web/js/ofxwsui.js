var socket;
var bConnected = false;
var $btConnect;
var $container;
var $currGroupContainer;
var components = [];
var bLock = false;
var onEvent = null;
var onConnectCallback = null;
var onDisonnectCallback = null;


function Slider(options) {
  options = options || {};
  var self = this;
  this.$view = null;
  this.params = {};
}

Slider.prototype.param = function(){

};


function connect(){
  $btConnect.html('Waiting..')
    .prop("disabled", true)
    .addClass('btn-primary-highlight');

  socket = new WebSocket('ws://' + window.location.host);
  socket.onopen = onConnectedHandler;
  socket.onclose = onDisconnectedHandler;
  socket.onmessage = function(msg) {
    data = JSON.parse(msg.data);
    if (data.type == 'definition') {
      parseParamsFromDefinition(data.data);
    }
    else if (data.type == 'event') {
      if(onEvent) onEvent(data.name, data.data);
    }
    else if (data.type == 'param_update') {
      onUpdateReceived(data.data.name, data.data.value);
    }
  };
}

function parseParamsFromDefinition(data){
  addGroup(data.group);
  for (var i = 0; i < data.params.length; i++) {
    var p = data.params[i];
    switch(p.type) {
      case 'int':
        addSlider('integer', p.name, p.value, p.min, p.max);
        break;
      case 'float':
        addSlider('float', p.name, p.value, p.min, p.max);
        break;
      case 'bool':
        addToggle(p.name, p.name, p.value);
        break;
    }
  }
}

function disconnect(){
  $btConnect.html('Waiting..')
    .prop('disabled', true)
    .removeClass('btn-primary-highlight');
  disable();
  socket.close();
}

function disable(){
  $container.addClass('disabled');
  $container.find('.has-switch').bootstrapSwitch('setActive', false);
  $container.find('.btn').prop('disabled', true);
  // if($container.find(':checkbox').length) {
  //     $container.find(':checkbox').prop('disabled', true).data('checkbox').setState();
  // }
  $container.find('.ui-slider').slider('option', 'disabled', true);
}

function enable(){
  $container.removeClass('disabled');
  $container.find('.has-switch').bootstrapSwitch('setActive', true);
  $container.find('.btn').prop('disabled', false);
  // if($container.find(':checkbox').length) {
  //   $container.find(':checkbox').prop('disabled', false).data('checkbox').setState();
  // }
  $container.find('.ui-slider').slider('option', 'disabled', false);
}

//--- Controls

function addConnectButton(){
  var $bt = $('<button>')
        .attr('name', 'connect')
        .attr('id', 'bt_connect')
        .addClass('btn btn-primary btn-wide')
        .html('Connect');
  $currGroupContainer.append($bt);
}

function addComponent(options){

  var name = options.name;
  var label = options.label;
  var $comp = options.comp;
  var $compContainer = options.compContainer;

  if($compContainer===undefined){
    $compContainer = $comp;
  }

  var $compDiv = $('<div>')
        .addClass('comp-content')
        .append($compContainer);

  var $p = $('<p>').html(name);

  var $nameDiv = $('<div>')
        .addClass('comp-title')
        .append($p);

  $('<div>')
    .addClass('comp-row')
    .append($nameDiv)
    .append($compDiv)
    .appendTo($currGroupContainer);

    components.push($comp);
}

function addSlider(type, name, value, min, max, label){
  var $slider = $('<div>')
        .addClass('ui-slider')
        .attr('id', name)
        .data('type', type)
        .slider({
          min: min,
          max: max,
          value: value,
          orientation: "horizontal",
          range: "min",
          slide: sliderChanged,
          start: sliderStarted,
          stop: sliderStopped,
          step: Math.abs(max-min)/150
        });
  addComponent({name:name, label:label, comp:$slider});
}

function addCheckbox(name, label){
  var $checkbox = $('<input>')
        .attr('type', 'checkbox')
        .attr('id', name)
        .attr('data-toggle', 'checkbox');
  
  var $label = $('<label class="checkbox">')
        .attr('for', name)
        .append($checkbox)
        .append(name);

  addComponent({name:name, label:label, comp:$checkbox, compContainer:$label});
  
  $checkbox.checkbox().on('toggle', checkboxChanged);
}

function addToggle(name, label, value){
  var $toggle = $('<input>')
        .addClass('switch-min')
        .attr('type', 'checkbox')
        .attr('checked', value)
        .attr('id', name)
        .attr('data-toggle', 'switch');
  var $div = $('<div>')
        .addClass('switch')
        .append($toggle);
  
  addComponent({name:name, label:label, comp:$toggle, compContainer:$div});

  $div.bootstrapSwitch().on('change', toggleChanged);
}

function addSelect(name, values, bMultiple, label){
  var $select = $("<select>").attr('id', name);
  for (var i = 0; i < values.length; i++) {
    $select.append('<option value='+i+'>'+values[i]+'</option>');
  }
  if(bMultiple) $select.attr('multiple', 'multiple');

  addComponent({name:name, label:label, comp:$select});
  
  $select.selectpicker({style: 'btn-primary', menuStyle: 'dropdown-inverse'});

  $select.on('change', selectChanged);
}

function addGroup(name){
  $currGroupContainer = $('<div class="demo-content">');
  
  var $title = $('<div class="demo-title">');
  var $label = $('<h3>').html(name);
  $title.append($label);

  var $row = $('<div class="demo-row">');
  $row.append($title);
  $row.append($currGroupContainer);
  $container.append('<hr>');
  $container.append($row);
}

//--- Socket Events

function onConnectedHandler(){
  bConnected = true;
  enable();
  $btConnect.html('Disconnect').prop("disabled", false);
  if(onConnectCallback) onConnectCallback();
}

function onDisconnectedHandler(){
  bConnected = false;
  disable();
  $btConnect.html('Connect').prop("disabled", false);
  if(onDisonnectCallback) onDisonnectCallback();
}

function onUpdateReceived(name, val){

  for (var i = components.length - 1; i >= 0; i--) {
    var $comp = components[i];
    if($comp.attr('id') == name){
      if($comp.attr('type') == 'checkbox') {
        if($comp.hasClass('has-switch') || $comp.hasClass('switch-min')){ // toggle
          bLock = true;
          $comp.parent().bootstrapSwitch('setState', val, true, true);
          bLock = false;
        } else { // toggle
          $comp.prop('checked', val).data('checkbox').setState();
        }
      } else {
        var sliderVal = $comp.slider("option", "value");
        if(!bLock){
          $comp.slider("option", "value", val);
        }
      }
    }
  }
}

//--- Socket methods

function sendSliderValue($slider){
  var type = $slider.data('type');
  var value = $slider.slider('option', 'value');
  if(type=="integer") {
    value = Math.round(value);
  }
  socket.send(JSON.stringify({
    name: $slider.attr('id'),
    value: value,
  }));
}

//--- UI Events

function sliderStarted(){
  bLock = true;
}

function sliderStopped(){
  bLock = false;
  sendSliderValue( $(this) );
}

function sliderChanged(){
  sendSliderValue( $(this) );
}

function checkboxChanged(ev){
  if(bLock) return;
  var $check = $(this);
  socket.send(JSON.stringify({
    name: $check.attr('id'),
    value: ev.checked
  }));
}

function toggleChanged(ev){
  if(bLock) return;
  var $toggle = $(this);
  var val = $toggle.bootstrapSwitch('status');
  socket.send(JSON.stringify({
    name: $toggle.find('input').attr('id'),
    value: val
  }));
}

function selectChanged(ev){
  var $select = $(this);
  var val = $select.selectpicker('val');
  
  if(typeof val != 'object')
    console.log($select[0][val].innerHTML);
}